SE3_ATE_RMSE[cm]: 15.646118479464
SE3_REL_TRANSLATION_0.5M[%]: 24.009219674806
SE3_REL_ROTATION_0.5M[deg/m]: 9.9069597146562
WARNING: Found less than 50 subsequences to evaluate relative errors for evaluation_distance 1. The results may be too noisy.
SE3_REL_TRANSLATION_1M[%]: 3.5226329116571
SE3_REL_ROTATION_1M[deg/m]: 1.5251756487445

SIM3_ATE_RMSE[cm]: 14.899244394665
SIM3_REL_TRANSLATION_0.5M[%]: 26.918982729107
SIM3_REL_ROTATION_0.5M[deg/m]: 9.9069597146562
WARNING: Found less than 50 subsequences to evaluate relative errors for evaluation_distance 1. The results may be too noisy.
SIM3_REL_TRANSLATION_1M[%]: 7.4339960385893
SIM3_REL_ROTATION_1M[deg/m]: 1.5251756487445

SE3_ATE_RMSE[cm]: 13.506830430199
SE3_REL_TRANSLATION_0.5M[%]: 29.373293514366
SE3_REL_ROTATION_0.5M[deg/m]: 11.092832817407
WARNING: Found less than 50 subsequences to evaluate relative errors for evaluation_distance 1. The results may be too noisy.
SE3_REL_TRANSLATION_1M[%]: 6.7120496754117
SE3_REL_ROTATION_1M[deg/m]: 2.1107499192565

SIM3_ATE_RMSE[cm]: 13.328603715134
SIM3_REL_TRANSLATION_0.5M[%]: 31.484320396645
SIM3_REL_ROTATION_0.5M[deg/m]: 11.092832817407
WARNING: Found less than 50 subsequences to evaluate relative errors for evaluation_distance 1. The results may be too noisy.
SIM3_REL_TRANSLATION_1M[%]: 9.5920968240202
SIM3_REL_ROTATION_1M[deg/m]: 2.1107499192565

SE3_ATE_RMSE[cm]: 16.828862447764
SE3_REL_TRANSLATION_0.5M[%]: 25.151965912832
SE3_REL_ROTATION_0.5M[deg/m]: 13.868514776674
WARNING: Found less than 50 subsequences to evaluate relative errors for evaluation_distance 1. The results may be too noisy.
SE3_REL_TRANSLATION_1M[%]: 24.595349884757
SE3_REL_ROTATION_1M[deg/m]: 10.642616866837

SIM3_ATE_RMSE[cm]: 15.263613799562
SIM3_REL_TRANSLATION_0.5M[%]: 29.535819130062
SIM3_REL_ROTATION_0.5M[deg/m]: 13.868514776674
WARNING: Found less than 50 subsequences to evaluate relative errors for evaluation_distance 1. The results may be too noisy.
SIM3_REL_TRANSLATION_1M[%]: 24.71239229149
SIM3_REL_ROTATION_1M[deg/m]: 10.642616866837

SE3_ATE_RMSE[cm]: 3.1530457378739
SE3_REL_TRANSLATION_0.5M[%]: 4.5417617047794
SE3_REL_ROTATION_0.5M[deg/m]: 2.7692863948162
WARNING: Found less than 50 subsequences to evaluate relative errors for evaluation_distance 1. The results may be too noisy.
SE3_REL_TRANSLATION_1M[%]: 1.0259991175486
SE3_REL_ROTATION_1M[deg/m]: 2.0666407710258

SIM3_ATE_RMSE[cm]: 2.2603661448149
SIM3_REL_TRANSLATION_0.5M[%]: 5.6245957395982
SIM3_REL_ROTATION_0.5M[deg/m]: 2.7692863948162
WARNING: Found less than 50 subsequences to evaluate relative errors for evaluation_distance 1. The results may be too noisy.
SIM3_REL_TRANSLATION_1M[%]: 3.4966806427878
SIM3_REL_ROTATION_1M[deg/m]: 2.0666407710258

SE3_ATE_RMSE[cm]: 20.97399093764
SE3_REL_TRANSLATION_0.5M[%]: 36.704657143529
SE3_REL_ROTATION_0.5M[deg/m]: 41.321840405379
WARNING: Found less than 50 subsequences to evaluate relative errors for evaluation_distance 1. The results may be too noisy.
SE3_REL_TRANSLATION_1M[%]: 31.918403365304
SE3_REL_ROTATION_1M[deg/m]: 51.667923955962

SIM3_ATE_RMSE[cm]: 18.863839761597
SIM3_REL_TRANSLATION_0.5M[%]: 30.670958922776
SIM3_REL_ROTATION_0.5M[deg/m]: 41.321840405379
WARNING: Found less than 50 subsequences to evaluate relative errors for evaluation_distance 1. The results may be too noisy.
SIM3_REL_TRANSLATION_1M[%]: 19.791050517262
SIM3_REL_ROTATION_1M[deg/m]: 51.667923955962

SE3_ATE_RMSE[cm]: 14.090002725728
SE3_REL_TRANSLATION_0.5M[%]: 22.200629649475
SE3_REL_ROTATION_0.5M[deg/m]: 9.2597298659889
WARNING: Found less than 50 subsequences to evaluate relative errors for evaluation_distance 1. The results may be too noisy.
SE3_REL_TRANSLATION_1M[%]: 0.90970283567659
SE3_REL_ROTATION_1M[deg/m]: 1.2363733741145

SIM3_ATE_RMSE[cm]: 13.809470919293
SIM3_REL_TRANSLATION_0.5M[%]: 25.016931863145
SIM3_REL_ROTATION_0.5M[deg/m]: 9.2597298659889
WARNING: Found less than 50 subsequences to evaluate relative errors for evaluation_distance 1. The results may be too noisy.
SIM3_REL_TRANSLATION_1M[%]: 5.2842662282964
SIM3_REL_ROTATION_1M[deg/m]: 1.2363733741145

SE3_ATE_RMSE[cm]: 12.259382441309
SE3_REL_TRANSLATION_0.5M[%]: 23.352851022621
SE3_REL_ROTATION_0.5M[deg/m]: 8.6641650955307
WARNING: Found less than 50 subsequences to evaluate relative errors for evaluation_distance 1. The results may be too noisy.
SE3_REL_TRANSLATION_1M[%]: 1.9499677199971
SE3_REL_ROTATION_1M[deg/m]: 1.1867512142971

SIM3_ATE_RMSE[cm]: 12.204094049449
SIM3_REL_TRANSLATION_0.5M[%]: 23.851640703905
SIM3_REL_ROTATION_0.5M[deg/m]: 8.6641650955307
WARNING: Found less than 50 subsequences to evaluate relative errors for evaluation_distance 1. The results may be too noisy.
SIM3_REL_TRANSLATION_1M[%]: 2.2116121511082
SIM3_REL_ROTATION_1M[deg/m]: 1.1867512142971

SE3_ATE_RMSE[cm]: 28.087425813846
SE3_REL_TRANSLATION_0.5M[%]: 46.188068940221
SE3_REL_ROTATION_0.5M[deg/m]: 53.028604767134
WARNING: Found less than 50 subsequences to evaluate relative errors for evaluation_distance 1. The results may be too noisy.
SE3_REL_TRANSLATION_1M[%]: 43.405029173157
SE3_REL_ROTATION_1M[deg/m]: 55.762995026296

SIM3_ATE_RMSE[cm]: 19.509992808034
SIM3_REL_TRANSLATION_0.5M[%]: 23.934862406684
SIM3_REL_ROTATION_0.5M[deg/m]: 53.028604767134
WARNING: Found less than 50 subsequences to evaluate relative errors for evaluation_distance 1. The results may be too noisy.
SIM3_REL_TRANSLATION_1M[%]: 17.09257444182
SIM3_REL_ROTATION_1M[deg/m]: 55.762995026296

SE3_ATE_RMSE[cm]: 14.666522371236
SE3_REL_TRANSLATION_0.5M[%]: 22.782423061127
SE3_REL_ROTATION_0.5M[deg/m]: 9.2504848879395
WARNING: Found less than 50 subsequences to evaluate relative errors for evaluation_distance 1. The results may be too noisy.
SE3_REL_TRANSLATION_1M[%]: 2.2908097229896
SE3_REL_ROTATION_1M[deg/m]: 1.0584374531715

SIM3_ATE_RMSE[cm]: 14.143355404491
SIM3_REL_TRANSLATION_0.5M[%]: 25.825186423909
SIM3_REL_ROTATION_0.5M[deg/m]: 9.2504848879395
WARNING: Found less than 50 subsequences to evaluate relative errors for evaluation_distance 1. The results may be too noisy.
SIM3_REL_TRANSLATION_1M[%]: 6.6202960214567
SIM3_REL_ROTATION_1M[deg/m]: 1.0584374531715

SE3_ATE_RMSE[cm]: 20.6310550352
SE3_REL_TRANSLATION_0.5M[%]: 29.029971393923
SE3_REL_ROTATION_0.5M[deg/m]: 15.576289391929
WARNING: Found less than 50 subsequences to evaluate relative errors for evaluation_distance 1. The results may be too noisy.
SE3_REL_TRANSLATION_1M[%]: 12.878546023558
SE3_REL_ROTATION_1M[deg/m]: 9.388448597776

SIM3_ATE_RMSE[cm]: 18.597670462371
SIM3_REL_TRANSLATION_0.5M[%]: 40.2838137989
SIM3_REL_ROTATION_0.5M[deg/m]: 15.576289391929
WARNING: Found less than 50 subsequences to evaluate relative errors for evaluation_distance 1. The results may be too noisy.
SIM3_REL_TRANSLATION_1M[%]: 17.545387506301
SIM3_REL_ROTATION_1M[deg/m]: 9.388448597776

