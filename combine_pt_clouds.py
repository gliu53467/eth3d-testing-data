# !/usr/bin/python

# # import os
# import sys
import subprocess
#
# # Folder to open
# folder = sys.argv[1]
# # folder = "sample_data"
# f_OS2 = folder + "/OS2_results/"
# f_OS3 = folder + "/OS3_results/"
# gt_file = folder+"/groundtruth.txt"
#
#
# # subprocess.run(["rm", "data.txt"])
# # subprocess.run(["touch", "data.txt"])
# print(f_OS2)
# print(f_OS3)
# print(gt_file)
#
# f = open("data.txt", "w")
#
#
# for i in range(10):
#     args1 = f_OS2+"CameraTrajectory_"+str(i+1)+".txt"
#     # print(args1)
#     list_files = subprocess.call(["python","compare_trajectories.py", args1, gt_file], stdout=f)
#     # list_files = subprocess.run(["python","compare_trajectories.py", args1, gt_file, ">>", "data.txt"])
#
#
# for i in range(10):
#     args1 = f_OS3+"CameraTrajectory_"+str(i+1)+".txt"
#     list_files = subprocess.call(["python","compare_trajectories.py", args1, gt_file], stdout=f)
#     # print("The exit code was: %d" % list_files.returncode)
#
# f.close()

file_paths = ["/media/gordon/T7/Datasets/test_ETH3D/ETH3D Testing Data/mannequin_head","/media/gordon/T7/Datasets/test_ETH3D/ETH3D Testing Data/plant_2", "/media/gordon/T7/Datasets/test_ETH3D/ETH3D Testing Data/plant_3"]

for path in file_paths:
    # ORB-SLAM 2 results
    f_OS2 = path + "/OS2_results/"
    write_path  = f_OS2+"pt_combined.ply"
    subprocess.call(["rm", write_path])
    f_write = open(write_path, "w")

    for j in range(10):
        read_path = f_OS2 + "pt_cloud_output_" + str(j+1) + ".ply"
        f_read = open(read_path, "r")
        buffer = f_read.readlines()

        if j > 0:
            del buffer[0:11]
    
        f_write.writelines(buffer)

        buffer.clear()
        f_read.close()

    f_write.close()


    # ORB-SLAM 3 results
    f_OS3 = path + "/OS3_results/"
    write_path  = f_OS3+"pt_combined.ply"
    subprocess.call(["rm", write_path])
    f_write = open(write_path, "w")

    for j in range(10):
        read_path = f_OS3 + "pt_cloud_output_" + str(j+1) + ".ply"
        f_read = open(read_path, "r")
        buffer = f_read.readlines()

        if j > 0:
            del buffer[0:11]
        f_write.writelines(buffer)

        buffer.clear()
        f_read.close()

    f_write.close()
