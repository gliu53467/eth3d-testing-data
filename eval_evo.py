import sys
import subprocess
import os


# Folder to open
folder = os.path.abspath(os.getcwd())+"/"+sys.argv[1]+"/"
# folder = "sample_data"

test_name = "test_num_ORB"
test_yaml = folder+"/ORB_features_test.yaml"

f_OS2 = folder + test_name + "_OS2/"
f_OS3 = folder + test_name + "_OS3/"
gt_file = folder+"/groundtruth.txt"
rgb_file = folder+"/rgb.txt"
eval_tool = folder + "/ETH3DSLAMEvaluation"
# print(folder)

settings_file = folder+"/ETH3D_RGBD.yaml"
associated_file = folder+"/associated.txt"
# ape_args1 = ["mkdir", f_OS2, f_OS3]
# subprocess.call(ape_args1)
results_folder = "results_"+sys.argv[1]

subprocess.call(["mkdir", results_folder])
with open(results_folder+"/OS 2 results_"+sys.argv[2]+".txt","w+") as out, open("stderr.txt","wb") as err:
    for i in range(10):
        ct = f_OS2+"CameraTrajectory_"+sys.argv[2]+"_"+str(i+1)+".txt"
        ape_args = [eval_tool, gt_file, ct, "-a"]
        ATE = subprocess.Popen((ape_args),stdout=out,stderr=err)
    for i in range(10):
        ct = f_OS2+"CameraTrajectory_"+sys.argv[2]+"_"+str(i+1)+".txt"
        rpe_args = ["evo_rpe", "tum", gt_file, ct, "-a"]
        RPE = subprocess.Popen((rpe_args),stdout=out,stderr=err)


with open(results_folder+"/OS 3 results_"+sys.argv[2]+".txt","w+") as out, open("stderr.txt","wb") as err:
    for i in range(10):
        ct = f_OS3+"CameraTrajectory_"+sys.argv[2]+"_"+str(i+1)+".txt"
        ape_args = ["evo_ape", "tum", gt_file, ct, "-a"]
        ATE = subprocess.Popen((ape_args),stdout=out,stderr=err)
    for i in range(10):
        ct = f_OS3+"CameraTrajectory_"+sys.argv[2]+"_"+str(i+1)+".txt"
        rpe_args = ["evo_rpe", "tum", gt_file, ct, "-a"]
        RPE = subprocess.Popen((rpe_args),stdout=out,stderr=err)


print("[evo] Done!!!")



# f1.close()
# f2.close()
