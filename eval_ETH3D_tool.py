import sys
import subprocess
import os, os.path

# Folder to evaulate
eval_folder = os.path.abspath(os.getcwd())+"/"+sys.argv[1]

# Create directory to store results
results_folder = os.path.abspath(os.getcwd())+"/Eval_Results/"+sys.argv[1]
subprocess.call(["mkdir", "-p", results_folder])

# Copy point cloud over as well -  not working
# pc_folder = "Eval_Results/"+sys.argv[1]+"/point clouds"
# subprocess.call(["mkdir","-p",pc_folder])
# # subprocess.call(['find', results_folder+'/results_OS3/ -type f |', 'grep -i ply$ |', 'xargs', '-i', 'cp', '{}', pc_folder])
# # subprocess.call(["find", results_folder+"/results_OS2/ -type f |","grep -i .ply$","|","xargs","-i","cp","{}", pc_folder])
# # subprocess.call(["find", results_folder+"/results_OS2", "-type", "f", "-name","'*.ply'", "-exec", "cp", "'{}'", pc_folder+"/", "';'"])
# # subprocess.call(["cd", eval_folder+"/results_OS3/", ":", "cp", "*.ply", pc_folder])

num_to_eval = 0
for filename in os.listdir(eval_folder):
    if "CameraTrajectory" in filename:
        print(filename)
        num_to_eval += 1

test_name = "results"

f_OS2 = eval_folder + test_name + "_OS2/"
f_OS3 = eval_folder + test_name + "_OS3/"
gt_file = eval_folder+"/groundtruth.txt"
rgb_file = eval_folder+"/rgb.txt"
eval_tool = os.path.abspath(os.getcwd())+"/ETH3DSLAMEvaluation"

settings_file = eval_folder+"/ETH3D_RGBD.yaml"
associated_file = eval_folder+"/associated.txt"

with open(results_folder+"/OS2_results.txt","w") as out, open("stderr.txt","wb") as err:
    for i in range(num_to_eval):
        ct = f_OS3+"CameraTrajectory_"+str(i+1)+".txt"
        args = [eval_tool, gt_file, ct, rgb_file]
        ATE = subprocess.Popen((args),stdout=out,stderr=err)
        ATE.wait()

with open(results_folder+"/OS3_results.txt","w") as out, open("stderr.txt","wb") as err:
    for i in range(num_to_eval):
        ct = f_OS3+"CameraTrajectory_"+str(i+1)+".txt"
        args = [eval_tool, gt_file, ct, rgb_file]
        ATE = subprocess.Popen((args),stdout=out,stderr=err)
        ATE.wait()

print("[ETH3DSLAMEvaluation] Done!!!")


