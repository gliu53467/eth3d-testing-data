# !/usr/bin/python

# import os
import sys
import subprocess

# Folder to open
folder = sys.argv[1]
# folder = "sample_data"
f_OS2 = folder + "/OS2_results/"
f_OS3 = folder + "/OS3_results/"
gt_file = folder+"/groundtruth.txt"


# subprocess.run(["rm", "data.txt"])
# subprocess.run(["touch", "data.txt"])
# print(f_OS2)
# print(f_OS3)
# print(gt_file)

f = open("data.txt", "w")


for i in range(10):
    args1 = f_OS2+"CameraTrajectory_"+str(i+1)+".txt"
    # print(args1)
    list_files = subprocess.call(["python3","compare_trajectories.py", args1, gt_file], stdout=f)
    # list_files = subprocess.run(["python","compare_trajectories.py", args1, gt_file, ">>", "data.txt"])
print("OS2 done")
# f.write("\n")

for i in range(10):
    args1 = f_OS3+"CameraTrajectory_"+str(i+1)+".txt"
    list_files = subprocess.call(["python3","compare_trajectories.py", args1, gt_file], stdout=f)
    # print("The exit code was: %d" % list_files.returncode)
print("OS3 done")
print("Saved to data.txt")

f.close()
